import prisma from '../utils/db.utils';


export class Users {
    static async getUserById(event): Promise<any> {
        return new Promise(async (resolve, reject) => {
            try {
                const user_id = +event.pathParameters.id;
                let where= {};
                if(isNaN(user_id)){
                    where={
                        email:event.pathParameters.id
                    }
                }else{
                    where={
                        id:user_id
                    }
                }
                const users = await prisma.users.findMany({
                    where:where,
                    select: {
                        id: true,
                        name: true,
                        email: true,
                        image: true,
                        posts: {
                            select: {
                                id: true,
                                title: true,
                                post: true,
                                cover_image: true,
                                created_at: true,
                                likes_likesToposts: {
                                    select: {
                                        id: true,
                                        users: {
                                            select: {
                                                id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                },
                                comments: {
                                    select: {
                                        id: true,
                                        comment: true,
                                        created_at: true,
                                        users: {
                                            select: {
                                                id: true,
                                                name: true,
                                                email: true,
                                                image: true,
                                            }
                                        }
                                    }
                                },
                                users: {
                                    select: {
                                        id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                        followers_followers_followed_byTousers: {
                                            select: {
                                                users_followers_followingTousers: {
                                                    select: {
                                                        id: true,
                                                        name: true,
                                                        email: true,
                                                        image: true,
                                                    }
                                                }
                                            }
                                        },
                                        followers_followers_followingTousers: {
                                            select: {
                                                users_followers_followed_byTousers: {
                                                    select: {
                                                        id: true,
                                                        name: true,
                                                        email: true,
                                                        image: true,
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        followers_followers_followed_byTousers: {
                            select: {
                                users_followers_followingTousers: {
                                    select: {
                                        id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        },
                        followers_followers_followingTousers: {
                            select: {
                                users_followers_followed_byTousers: {
                                    select: {
                                        id: true,
                                        name: true,
                                        email: true,
                                        image: true,
                                    }
                                }
                            }
                        }
                    }
                });
                resolve(users[0]);
            } catch (err) {
                reject(err);
            }
        });
    }
}

