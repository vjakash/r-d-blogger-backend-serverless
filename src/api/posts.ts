/* eslint-disable @typescript-eslint/no-explicit-any /
/ eslint-disable @typescript-eslint/no-unused-vars */
import { Posts } from '../service/posts.service';
import { Users } from '../service/users.service';
import logger from '../utils/logging';
import { sendErrorResponse, sendSuccessResponse } from '../utils/response';

class PostsApi {
    static async getAllPosts(_event: any): Promise<any> {
        try {
            logger.info('Enterd getAllPosts');
            const posts = await Posts.getAllPosts(_event);
            return sendSuccessResponse(posts);
        } catch (err) {
            logger.error(err);
            return sendErrorResponse(err);
        }
    }
    static async getPostById(_event: any): Promise<any> {
        try {
            logger.info('Enterd getPostById');
            const post = await Posts.getPostById(_event);
            return sendSuccessResponse(post);
        } catch (err) {
            logger.error(err);
            return sendErrorResponse(err);
        }
    }
    static async postComment(_event: any): Promise<any> {
        try {
            logger.info('Enterd postComment');
            const post = await Posts.postComment(_event);
            return sendSuccessResponse(post);
        } catch (err) {
            logger.error(err);
            return sendErrorResponse(err);
        }
    }
    static async likePost(_event: any): Promise<any> {
        try {
            logger.info('Enterd likePost');
            const post = await Posts.likePost(_event);
            return sendSuccessResponse(post);
        } catch (err) {
            logger.error(err);
            return sendErrorResponse(err);
        }
    }
    static async followUser(_event: any): Promise<any> {
        try {
            logger.info('Enterd followUser');
            const post = await Posts.followUser(_event);
            return sendSuccessResponse(post);
        } catch (err) {
            logger.error(err);
            return sendErrorResponse(err);
        }
    }
    static async getUserById(_event: any): Promise<any> {
        try {
            logger.info('Enterd getUserById');
            const post = await Users.getUserById(_event);
            return sendSuccessResponse(post);
        } catch (err) {
            logger.error(err);
            return sendErrorResponse(err);
        }
    }
    static async createPost(_event: any): Promise<any> {
        try {
            logger.info('Enterd createPost');
            const post = await Posts.createPost(_event);
            return sendSuccessResponse(post);
        } catch (err) {
            logger.error(err);
            return sendErrorResponse(err);
        }
    }
}

export const getAllPosts = PostsApi.getAllPosts;
export const getPostById = PostsApi.getPostById;
export const postComment = PostsApi.postComment;
export const likePost = PostsApi.likePost;
export const followUser = PostsApi.followUser;
export const getUserById = PostsApi.getUserById;
export const createPost = PostsApi.createPost;
