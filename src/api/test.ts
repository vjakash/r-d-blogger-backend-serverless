/* eslint-disable @typescript-eslint/no-explicit-any /
/ eslint-disable @typescript-eslint/no-unused-vars */
import  logger  from '../utils/logging';
import { sendErrorResponse, sendSuccessResponse } from '../utils/response';

class TestApi {
  static async healthCheck(_event: any): Promise<any> {
      try {
        logger.info('Enterd healthCheck');       
        return sendSuccessResponse("Go serverless")
      } catch (err) {
        logger.error(err);
        return sendErrorResponse(err);
      }
  }
}

export const healthCheck = TestApi.healthCheck;