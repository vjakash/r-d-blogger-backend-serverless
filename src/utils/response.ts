
const headers = {
  'Access-Control-Allow-Origin': '*',
};

export function sendSuccessResponse(body: any) {
  return {
    headers,
    statusCode: 200,
    body: JSON.stringify({
      body,
    }),
  };
}

export function sendErrorResponse(error: Error) {
  return {
    headers,
    statusCode: 400,
    body: JSON.stringify({
      error,
    }),
  };
}
